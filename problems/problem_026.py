# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    if len(values) == 0:
        return None

    sum = 0
    for each in values:
        sum += each

    grade = sum / len(values)

    if grade >= 90:
        return "grade=A"

    elif grade >= 80:
        return "grade = B"

    elif grade >= 70:
        return "grade = C"

    elif grade >= 60:
        return "grade = D"

    else:
        return "grade = F"


scores = [95, 90, 97, 91, 92, 100]

print(calculate_grade(scores))
