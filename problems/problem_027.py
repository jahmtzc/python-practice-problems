# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None

    for each in values:
        return max(values)


list1 = [10, 11, 12, 13, 14]

print(max_in_list(list1))
