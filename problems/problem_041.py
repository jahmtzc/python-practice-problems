# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    newlist = []
    for each in csv_lines:
        sepnum = each.split(",")
        csvsum = 0
        for nums in sepnum:
            numsint = int(nums)
            csvsum += numsint
        newlist.append(csvsum)
    return newlist


input1 = ["3", "1,9"]

print(add_csv_lines(input1))
