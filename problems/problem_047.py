# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    num_lowers = 0
    num_uppers = 0
    num_specs = 0
    num_dig = 0
    critsum = 0
    pw_string = str(password)
    pw_list = list(pw_string)
    if len(pw_list) >= 6 and len(pw_list) <= 12:
        critsum += 1
    for each in pw_list:
        if each.islower() is True:
            num_lowers += 1
        elif each.isupper() is True:
            num_uppers += 1
        elif each.isdigit() is True:
            num_dig += 1
        elif each == "$" or each == "!" or each == "@":
            num_specs += 1

    if num_lowers >= 1:
        critsum += 1
    if num_uppers >= 1:
        critsum += 1
    if num_dig >= 1:
        critsum += 1
    if num_specs >= 1:
        critsum += 1
    if critsum == 5:
        return True
    else:
        return False


print(check_password("Fart123@"))
