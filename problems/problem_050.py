# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(one_list):
    first_half = []
    second_half = []
    half_length = int(len(one_list) // 2) + (len(one_list) % 2)
    for each in range(half_length):
        first_half.append(one_list[each])

    for i in range(len(one_list) // 2):
        index = i + half_length
        second_half.append(one_list[index])
    return first_half, second_half


print(halve_the_list([1, 2, 3]))
